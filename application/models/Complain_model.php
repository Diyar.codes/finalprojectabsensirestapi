<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Complain_model extends CI_Model
{
    function getComplain($id, $idUser)
    {
        $this->db->where('id_user', $idUser);
        if ($id == null) {
            $complain = $this->db->get('tbl_complain')->result();
        } else {
            $this->db->where('id_complain', $id);
            $complain = $this->db->get('tbl_complain')->result();
        }
        return $complain;
    }

    function getComplains($id)
    {
        if ($id == null) {
            $complain = $this->db->get('tbl_complain')->result();
        } else {
            $this->db->where('id_complain', $id);
            $complain = $this->db->get('tbl_complain')->row();
        }
        return $complain;
    }

    function countComplain()
    {
        $user = $this->db->get('tbl_complain')->num_rows();
        return $user;
    }

    function insertComplain($data)
    {
        $data = $this->db->insert('tbl_complain', $data);
        return $data;
    }

    function cekIdComplain($id)
    {
        $data = $this->db->get_where('tbl_complain', ['id_complain' => $id]);
        return $data->num_rows();
    }

    function deleteComplain($id)
    {
        $data = $this->db->delete('tbl_complain', ['id_complain' => $id]);
        return $data;
    }

    function updateComplain($id, $data)
    {
        $this->db->where('id_complain', $id);
        $update = $this->db->update('tbl_complain', $data);
        return $update;
    }
}
