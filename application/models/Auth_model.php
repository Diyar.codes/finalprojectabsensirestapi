<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    // finance
    function getFinance($id)
    {
        $this->db->where('level', 'finance');
        if ($id == null) {
            $user = $this->db->get('tbl_users')->result();
        } else {
            $this->db->where('id_user', $id);
            $user = $this->db->get('tbl_users')->row();
        }
        return $user;
    }

    // Karyawan
    function getAuth($id, $level)
    {
        $this->db->where('level', 'karyawan');
        if ($id == null) {
            $user = $this->db->get('tbl_users')->result();
        } else {
            $this->db->where('id_user', $id);
            $user = $this->db->get('tbl_users')->row();
        }
        return $user;
    }

    function countUser()
    {
        $this->db->where('level', 'karyawan');
        $user = $this->db->get('tbl_users')->num_rows();
        return $user;
    }

    function insertUser($data)
    {
        $insertUser = $this->db->insert('tbl_users', $data);
        return $insertUser;
    }

    function cekIdUser($id)
    {
        $data = $this->db->get_where('tbl_users', ['id_user' => $id]);
        return $data->num_rows();
    }

    function deleteUser($id)
    {
        $data = $this->db->delete('tbl_users', ['id_user' => $id]);
        return $data;
    }

    function deleteUserFromAbsensi($id)
    {
        $data = $this->db->delete('tbl_absensi', ['id_user' => $id]);
        return $data;
    }

    function updateUser($id, $data)
    {
        $this->db->where('id_user', $id);
        $update = $this->db->update('tbl_users', $data);
        return $update;
    }

    function ubahPassword($idUser, $new_password1)
    {
        $this->db->set('password', $new_password1);
        $this->db->where('id_user', $idUser);
        $data = $this->db->update('tbl_users');
        return $data;
    }

    // Auth
    function getUserByEmail($email)
    {
        $getEmail = $this->db->get_where('tbl_users', ['email' => $email]);
        return $getEmail->row_array();
    }
}
