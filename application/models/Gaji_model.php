<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Gaji_model extends CI_Model
{
    function getCatatan($idUser, $bulan, $tahun)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('bulan', $bulan);
        $this->db->where('tahun', $tahun);
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function getDataAbsenMasuk($idUser, $bulan, $tahun)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('bulan', $bulan);
        $this->db->where('tahun', $tahun);
        $this->db->where('ket', 'masuk');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function getDataAbsenTelat($idUser, $bulan, $tahun)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('bulan', $bulan);
        $this->db->where('tahun', $tahun);
        $this->db->where('ket', 'telat');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function getDataAbsenMangkir($idUser, $bulan, $tahun)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('bulan', $bulan);
        $this->db->where('tahun', $tahun);
        $this->db->where('ket', 'mangkir');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function inserGajiEmploye($data)
    {
        $insert = $this->db->insert('tbl_gaji', $data);
        return $insert;
    }

    function cekData($idUser, $bulan, $tahun)
    {
        $cek = $this->db->get_where('tbl_gaji', ['id_user' => $idUser, 'bulan' => $bulan, 'tahun' => $tahun]);
        return $cek->num_rows();
    }

    function getGaji($id, $idUser)
    {
        $this->db->order_by('id_gaji', 'DESC');
        $this->db->where('id_user', $idUser);
        if ($id == null) {
            $absensi = $this->db->get('tbl_gaji')->result();
        } else {
            $this->db->where('id_gaji', $id);
            $absensi = $this->db->get('tbl_gaji')->result();
        }
        return $absensi;
    }

    function cekIdGaji($id)
    {
        $data = $this->db->get_where('tbl_gaji', ['id_gaji' => $id]);
        return $data->num_rows();
    }

    function deleteGaji($id)
    {
        $data = $this->db->delete('tbl_gaji', ['id_gaji' => $id]);
        return $data;
    }

    function updateGaji($id, $data)
    {
        $this->db->where('id_gaji', $id);
        $update = $this->db->update('tbl_gaji', $data);
        return $update;
    }

    function getGajiById($id)
    {
        $data = $this->db->get_where('tbl_gaji', ['id_gaji' => $id]);
        return $data->row();
    }
}
