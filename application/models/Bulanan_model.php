<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bulanan_model extends CI_Model
{
    function getBulanan($idUser, $bulan, $tahun)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('bulan', $bulan);
        $this->db->where('tahun', $tahun);

        $absensi = $this->db->get('tbl_absensi')->result();
        return $absensi;
    }
}
