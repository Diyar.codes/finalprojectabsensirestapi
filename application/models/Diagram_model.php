<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Diagram_model extends CI_Model
{
    function countDiagramUser($idUser)
    {
        $this->db->where('id_user', $idUser);
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function countUserMasuk($idUser)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('ket', 'masuk');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function countUserTelat($idUser)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('ket', 'telat');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function countUserMangkir($idUser)
    {
        $this->db->where('id_user', $idUser);
        $this->db->where('ket', 'mangkir');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function countADiagramUser()
    {
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function countAUserMasuk()
    {
        $this->db->where('ket', 'masuk');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function countAUserTelat()
    {
        $this->db->where('ket', 'telat');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }

    function countAUserMangkir()
    {
        $this->db->where('ket', 'mangkir');
        $data = $this->db->get('tbl_absensi')->num_rows();
        return $data;
    }
}
