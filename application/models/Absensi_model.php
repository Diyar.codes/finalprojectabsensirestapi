<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi_model extends CI_Model
{
    function getAbsensi($id, $idUser)
    {
        $this->db->order_by('id_absensi', 'DESC');
        $this->db->where('id_user', $idUser);
        if ($id == null) {
            $absensi = $this->db->get('tbl_absensi')->result();
        } else {
            $this->db->where('id_absensi', $id);
            $absensi = $this->db->get('tbl_absensi')->result();
        }
        return $absensi;
    }

    function insertAbsensi($data)
    {
        $data = $this->db->insert('tbl_absensi', $data);
        return $data;
    }

    function cekData($idUser, $tgl, $bulan, $tahun)
    {
        $cek = $this->db->get_where('tbl_absensi', ['id_user' => $idUser, 'tgl' => $tgl, 'bulan' => $bulan, 'tahun' => $tahun]);
        return $cek->num_rows();
    }

    function updateAbsensi($id, $data)
    {
        $this->db->where('id_absensi', $id);
        $update = $this->db->update('tbl_absensi', $data);
        return $update;
    }

    function cekIdAbsensi($id)
    {
        $data = $this->db->get_where('tbl_absensi', ['id_absensi' => $id]);
        return $data->num_rows();
    }

    function deleteAbsensi($id)
    {
        $data = $this->db->delete('tbl_absensi', ['id_absensi' => $id]);
        return $data;
    }

    function getAbsensiCek($id)
    {
        $data = $this->db->get_where('tbl_absensi', ['id_absensi' => $id]);
        return $data->num_rows();
    }

    function getDataAbsensi($id)
    {
        $data = $this->db->get_where('tbl_absensi', ['id_absensi' => $id]);
        return $data->row();
    }
}
