<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Complain extends RestController
{
    function index_get()
    {
        $id = $this->get('id_complain');
        $idUser = $this->get('id_user');
        $complain = $this->Complain_model->getComplain($id, $idUser);
        if ($complain) {
            $this->response([
                'status' => true,
                'data' => $complain,
                'message' => 'complain found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $complain,
                'message' => 'complain not found'
            ], 404);
        }
    }

    function countComplain_get()
    {
        $user = $this->Complain_model->countComplain();
        if ($user) {
            $this->response([
                'status' => true,
                'data' => $user,
                'message' => 'complain total'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $user,
                'message' => 'complain not found'
            ], 404);
        }
    }

    function userComplain_get()
    {
        $id = $this->get('id_complain');
        $complain = $this->Complain_model->getComplains($id);
        if ($complain) {
            $this->response([
                'status' => true,
                'data' => $complain,
                'message' => 'complain found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'complain not found'
            ], 404);
        }
    }

    function index_post()
    {
        $id = $this->post('id_user');
        $id_gaji = $this->post('id_gaji');

        $gaji = $this->Gaji_model->getGajiById($id_gaji);
        $user = $this->Auth_model->getAuth($id);

        $data = [
            'id_user' => $id,
            'username' => $user->username,
            'pesan' => $this->input->post('pesan'),
            'id_gaji' => $this->post('id_gaji'),
            'bulan' => $gaji->bulan,
            'tahun' => $gaji->tahun
        ];

        $this->form_validation->set_rules('pesan', 'pesan', 'required');

        if ($this->form_validation->run() == false) {
            if (form_error('pesan')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('pesan')
                ], 404);
            }
        }

        if ($this->Complain_model->insertComplain($data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'successfully Complain'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'failed Complaint'
            ], 404);
        }
    }

    function index_delete()
    {
        $id = $this->delete('id_complain');

        if ($this->Complain_model->cekIdComplain($id) == 0) {
            $this->response([
                'status' => false,
                'message' => 'attendance id not found'
            ], 404);
        }

        if ($this->Complain_model->deleteComplain($id) == true) {
            $this->response([
                'status' => true,
                'data' => $id,
                'message' => 'absen deleted successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'absen failed to delete'
            ], 404);
        }
    }

    function index_put()
    {
        $id = $this->input->post('id_complain');
        $idUser = $this->input->post('id_user');
        $idGaji = $this->input->post('id_gaji');

        $user = $this->Auth_model->getAuth($idUser);
        $gaji = $this->Gaji_model->getGajiById($idGaji);

        $data = [
            'id_user' => $idUser,
            'username' => $user->username,
            'pesan' => htmlspecialchars($this->input->post('pesan')),
            'id_gaji' => $idGaji,
            'bulan' => $gaji->bulan,
            'tahun' => $gaji->tahun
        ];

        $this->form_validation->set_rules('pesan', 'Keterangan', 'required');

        if ($this->form_validation->run() == false) {
            if (form_error('pesan')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('pesan')
                ], 404);
            }
        }

        if ($this->Complain_model->updateComplain($id, $data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'Data changed successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data has failed to change'
            ], 404);
        }
    }
}
