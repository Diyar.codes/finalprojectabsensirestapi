<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use \Firebase\JWT\JWT;

class Auth extends RestController
{
    function index_post()
    {
        $data = [
            'email' => htmlspecialchars($this->input->post('email', true)),
            'password' => htmlspecialchars($this->input->post('password', true))
        ];

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            if (form_error('email')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('email')
                ], 404);
            } else if (form_error('password')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('password')
                ], 404);
            }
        }

        $getUser = $this->Auth_model->getUserByEmail($data['email']);


        if (empty($getUser)) {
            $this->response([
                'status' => false,
                'message' => 'Email belum terdaftar'
            ], 404);
        }

        if (!password_verify($data['password'], $getUser['password'])) {
            $this->response([
                'status' => false,
                'message' => 'Password salah'
            ], 404);
        }

        $key = "example_key";
        $payload = array(
            "iss"      => "http://example.org",
            "aud"      => "http://example.com",
            "iat"      => 1356999524,
            "nbf"      => 1357000000,
            "id_user"  => $getUser['id_user'],
            "email"    => $getUser['email'],
            "username"    => $getUser['username'],
            "level" => $getUser['level']
        );

        $jwt = JWT::encode($payload, $key);

        $this->response([
            'status'  => true,
            'message' => 'Login is successful, please enter the token',
            'token'   => $jwt,
            'data'    => $getUser
        ], 200);
    }
}
