<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Users extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('Validation');
        $this->validation->validationToken();
    }

    function index_get()
    {
        $id = $this->get('id_user');
        $user = $this->Auth_model->getAuth($id);
        if ($user) {
            $this->response([
                'status' => true,
                'data' => $user,
                'message' => 'user found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $user,
                'message' => 'user not found'
            ], 404);
        }
    }

    function finance_get()
    {
        $id = $this->get('id_user');
        $user = $this->Auth_model->getFinance($id);
        if ($user) {
            $this->response([
                'status' => true,
                'data' => $user,
                'message' => 'finance found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $user,
                'message' => 'finance not found'
            ], 404);
        }
    }

    function index_post()
    {
        $data = [
            'username'  => htmlspecialchars($this->post('username', true)),
            'email'     => htmlspecialchars($this->post('email', true)),
            'password'  => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'level'     => 'karyawan',
            'image'     => 'default.png'
        ];

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        $this->form_validation->set_rules('passwordConfirm', 'Password Confirmation', 'required|matches[password]');

        if ($this->form_validation->run() == false) {
            if (form_error('username')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('username')
                ], 404);
            } else if (form_error('email')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('email')
                ], 404);
            } else if (form_error('password')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('password')
                ], 404);
            } else if (form_error('passwordConfirm')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('passwordConfirm')
                ], 404);
            }
        }

        if ($this->Auth_model->insertUser($data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'user has successfully registered'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'user failed to register'
            ], 404);
        }
    }

    function index_delete()
    {
        $id = $this->delete('id_user');

        if ($this->Auth_model->cekIdUser($id) == 0) {
            $this->response([
                'status' => false,
                'message' => 'id User tidak ditemukan'
            ], 404);
        }

        $this->Auth_model->deleteUserFromAbsensi($id);

        if ($this->Auth_model->deleteUser($id) == true) {
            $this->response([
                'status' => true,
                'data' => $id,
                'message' => 'user deleted successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'user failed to delete'
            ], 404);
        }
    }

    function index_put()
    {
        $id = $this->input->post('id_user');

        $data = [
            'username'  => htmlspecialchars($this->input->post('username', true)),
            'email' => htmlspecialchars($this->input->post('email', true))
        ];

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == false) {
            if (form_error('username')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('username')
                ], 404);
            } else if (form_error('email')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('email')
                ], 404);
            }
        }

        if ($this->Auth_model->updateUser($id, $data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'Data changed successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data has failed to change'
            ], 404);
        }
    }

    function changePassword_put()
    {
        $idUser = $this->input->post('id_user');

        $user = $this->Auth_model->getAuth($idUser);

        $currentPassword = $this->input->post('current_password');
        $new_password1 = $this->input->post('new_password1');

        $this->form_validation->set_rules('current_password', 'Current Password', 'required');
        $this->form_validation->set_rules('new_password1', 'New Password', 'required|min_length[8]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|matches[new_password1]');

        if ($this->form_validation->run() == false) {
            if (form_error('current_password')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('current_password')
                ], 404);
            } else if (form_error('new_password1')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('new_password1')
                ], 404);
            } else if (form_error('new_password2')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('new_password2')
                ], 404);
            }
        }

        if (password_verify($currentPassword, $user->password)) {
            if ($currentPassword == $new_password1) {
                $this->response([
                    'status' => false,
                    'message' => 'The new password cannot be the same as the old password'
                ], 404);
            } else {
                $password_hash = password_hash($new_password1, PASSWORD_DEFAULT);

                if ($this->Auth_model->ubahPassword($idUser, $password_hash) == true) {
                    $this->response([
                        'status' => true,
                        'message' => 'password changed successfully'
                    ], 200);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => '
                        password changed failed'
                    ], 404);
                }
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'your old password is wrong'
            ], 404);
        }
    }

    function changePasswordFinance_put()
    {
        $idUser = $this->input->post('id_user');

        $user = $this->Auth_model->getFinance($idUser);

        $currentPassword = $this->input->post('current_password');
        $new_password1 = $this->input->post('new_password1');

        $this->form_validation->set_rules('current_password', 'Current Password', 'required');
        $this->form_validation->set_rules('new_password1', 'New Password', 'required|min_length[8]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirm New Password', 'matches[new_password1]');

        if ($this->form_validation->run() == false) {
            if (form_error('current_password')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('current_password')
                ], 404);
            } else if (form_error('new_password1')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('new_password1')
                ], 404);
            } else if (form_error('new_password2')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('new_password2')
                ], 404);
            }
        }

        if (password_verify($currentPassword, $user->password)) {
            if ($currentPassword == $new_password1) {
                $this->response([
                    'status' => false,
                    'message' => 'The new password cannot be the same as the old password'
                ], 404);
            } else {
                $password_hash = password_hash($new_password1, PASSWORD_DEFAULT);

                if ($this->Auth_model->ubahPassword($idUser, $password_hash) == true) {
                    $this->response([
                        'status' => true,
                        'message' => 'password changed successfully'
                    ], 200);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => '
                        password changed failed'
                    ], 404);
                }
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'your old password is wrong'
            ], 404);
        }
    }

    function changeUploadImage($gambarLama)
    {
        $config['upload_path']          = './assets/img/user';
        $config['allowed_types']        = 'jpeg|jpg|png';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            return $gambarLama;
        } else {
            if ($gambarLama != 'default.png') {
                unlink(FCPATH . '/assets/img/user/' . $gambarLama);
            }

            return $this->upload->data('file_name');
        }
    }

    function changeProfile_put()
    {
        $id = $this->input->post('id_user');

        $cekIdUser = $this->Auth_model->getAuth($id);
        $gambarLama = $cekIdUser->image;

        $data = [
            'username'  => htmlspecialchars($this->input->post('username', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'image'       => $this->changeUploadImage($gambarLama)
        ];

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == false) {
            if (form_error('username')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('username')
                ], 404);
            } else if (form_error('email')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('email')
                ], 404);
            }
        }

        if ($this->Auth_model->updateUser($id, $data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'personal data has been changed successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'personal data has failed to change'
            ], 404);
        }
    }

    function changeUploadImageFinance($gambarLama)
    {
        $config['upload_path']          = './assets/img/user';
        $config['allowed_types']        = 'jpeg|jpg|png';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            return $gambarLama;
        } else {
            if ($gambarLama != 'default.png') {
                unlink(FCPATH . '/assets/img/user/' . $gambarLama);
            }

            return $this->upload->data('file_name');
        }
    }

    function changeProfileFinance_put()
    {
        $id = $this->input->post('id_user');

        $cekIdUser = $this->Auth_model->getFinance($id);
        $gambarLama = $cekIdUser->image;

        $data = [
            'username'  => htmlspecialchars($this->input->post('username', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'image'       => $this->changeUploadImage($gambarLama)
        ];

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == false) {
            if (form_error('username')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('username')
                ], 404);
            } else if (form_error('email')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('email')
                ], 404);
            }
        }

        if ($this->Auth_model->updateUser($id, $data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'personal data has been changed successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'personal data has failed to change'
            ], 404);
        }
    }

    function countUser_get()
    {
        $user = $this->Auth_model->countUser();
        if ($user) {
            $this->response([
                'status' => true,
                'data' => $user,
                'message' => 'user total'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $user,
                'message' => 'user not found'
            ], 404);
        }
    }
}
