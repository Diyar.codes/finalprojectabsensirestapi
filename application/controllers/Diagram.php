<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Diagram extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('Validation');
        $this->validation->validationToken();
    }

    function index_get()
    {
        $idUser = $this->get('id_user');

        $diagramUser = $this->Diagram_model->countDiagramUser($idUser);
        $userMasuk = $this->Diagram_model->countUserMasuk($idUser);
        $userTelat = $this->Diagram_model->countUserTelat($idUser);
        $userMangkir = $this->Diagram_model->countUserMangkir($idUser);

        $masukD = $userMasuk / $diagramUser * 100;
        $telatD =  $userTelat / $diagramUser * 100;
        $mangkirD = $userMangkir / $diagramUser * 100;

        $data = [
            'masuk' => $masukD,
            'telat' => $telatD,
            'mangkir' => $mangkirD
        ];

        if ($data) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'diagram found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $data,
                'message' => 'diagram not found'
            ], 404);
        }
    }

    function AllEmployee_get()
    {
        $diagramUser = $this->Diagram_model->countADiagramUser();
        $userMasuk = $this->Diagram_model->countAUserMasuk();
        $userTelat = $this->Diagram_model->countAUserTelat();
        $userMangkir = $this->Diagram_model->countAUserMangkir();

        $masukD = $userMasuk / $diagramUser * 100;
        $telatD =  $userTelat / $diagramUser * 100;
        $mangkirD = $userMangkir / $diagramUser * 100;

        $data = [
            'masuk' => $masukD,
            'telat' => $telatD,
            'mangkir' => $mangkirD
        ];

        if ($data) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'diagram employee found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $data,
                'message' => 'diagram employee not found'
            ], 404);
        }
    }
}
