<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Absensibulanan extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
    }

    function index_post()
    {
        $idUser = $this->post('id_user');
        $bulan = $this->post('bulan');
        $tahun = $this->post('tahun');

        $this->form_validation->set_rules('bulan', 'Bulan', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');

        if ($this->form_validation->run() == false) {
            if (form_error('bulan')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('bulan')
                ], 404);
            } else if (form_error('tahun')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('tahun')
                ], 404);
            }
        }

        $absensi = $this->Bulanan_model->getBulanan($idUser, $bulan, $tahun);

        if ($absensi) {
            $this->response([
                'status' => true,
                'data' => $absensi,
                'message' => 'monthly data found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $absensi,
                'message' => 'monthly data not found'
            ], 404);
        }
    }
}
