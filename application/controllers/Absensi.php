<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Absensi extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('Validation');
        $this->validation->validationToken();
    }

    function index_get()
    {
        $id = $this->get('id_absensi');
        $idUser = $this->get('id_user');
        $absensi = $this->Absensi_model->getAbsensi($id, $idUser);
        if ($absensi) {
            $this->response([
                'status' => true,
                'data' => $absensi,
                'message' => 'attendance found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $absensi,
                'message' => 'attendance not found'
            ], 404);
        }
    }

    function index_post()
    {
        $id = $this->post('id_user');

        $user = $this->Auth_model->getAuth($id);

        $data = [
            'id_user' => $id,
            'username' => $user->username,
            'tgl' => $this->post('tgl'),
            'bulan' => $this->post('bulan'),
            'tahun' => $this->post('tahun'),
            'ket' => $this->post('ket')
        ];

        $this->form_validation->set_rules('tgl', 'Tanggal', 'required');
        $this->form_validation->set_rules('bulan', 'Bulan', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('ket', 'Keterangan', 'required');

        if ($this->form_validation->run() == false) {
            if (form_error('tgl')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('tgl')
                ], 404);
            } else if (form_error('bulan')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('bulan')
                ], 404);
            } else if (form_error('tahun')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('tahun')
                ], 404);
            } else if (form_error('ket')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('ket')
                ], 404);
            }
        }

        if ($this->Absensi_model->cekData($data['id_user'], $data['tgl'], $data['bulan'], $data['tahun']) == 1) {
            $this->response([
                'status' => false,
                'message' => 'pre-existing data'
            ], 404);
        }

        if ($this->Absensi_model->insertAbsensi($data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'successfully absent'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'failed absent'
            ], 404);
        }
    }

    function index_delete()
    {
        $id = $this->delete('id_absensi');

        if ($this->Absensi_model->cekIdAbsensi($id) == 0) {
            $this->response([
                'status' => false,
                'message' => 'attendance id not found'
            ], 404);
        }

        if ($this->Absensi_model->deleteAbsensi($id) == true) {
            $this->response([
                'status' => true,
                'data' => $id,
                'message' => 'absen deleted successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'absen failed to delete'
            ], 404);
        }
    }

    function index_put()
    {
        $id = $this->input->post('id_absensi');
        $idUser = $this->input->post('id_user');

        $user = $this->Auth_model->getAuth($idUser);

        $data = [
            'id_user' => $idUser,
            'username' => $user->username,
            'tgl' => $this->input->post('tgl'),
            'bulan' => $this->input->post('bulan'),
            'tahun' => $this->input->post('tahun'),
            'ket' => $this->input->post('ket')
        ];

        $this->form_validation->set_rules('tgl', 'Tanggal', 'required');
        $this->form_validation->set_rules('bulan', 'Bulan', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required');
        $this->form_validation->set_rules('ket', 'Keterangan', 'required');

        if ($this->form_validation->run() == false) {
            if (form_error('tgl')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('tgl')
                ], 404);
            } else if (form_error('bulan')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('bulan')
                ], 404);
            } else if (form_error('tahun')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('tahun')
                ], 404);
            } else if (form_error('ket')) {
                $this->response([
                    'status' => false,
                    'message' => form_error('ket')
                ], 404);
            }
        }

        // if ($this->Absensi_model->cekData($idUser, $data['tgl'], $data['bulan'], $data['tahun']) == 1) {
        //     $this->response([
        //         'status' => false,
        //         'message' => 'pre-existing data'
        //     ], 404);
        // }

        if ($this->Absensi_model->updateAbsensi($id, $data) == true) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'Data changed successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Data has failed to change'
            ], 404);
        }
    }

    function getDataAbsensi_get()
    {
        $id = $this->get('id_absensi');
        $absensi = $this->Absensi_model->getDataAbsensi($id);
        if ($absensi) {
            $this->response([
                'status' => true,
                'message' => 'attendance found',
                'data' => $absensi,
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'data' => $absensi,
                'message' => 'attendance not found'
            ], 404);
        }
    }
}
