<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Gaji extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('validation');
        $this->validation->validationToken();
    }

    function index_get()
    {
        $id = $this->get('id_gaji');
        $idUser = $this->get('id_user');
        $gaji = $this->Gaji_model->getGaji($id, $idUser);
        if ($gaji) {
            $this->response([
                'status' => true,
                'data' => $gaji,
                'message' => 'attendance found'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'attendance not found'
            ], 404);
        }
    }

    function index_post()
    {
        $idUser = $this->post('id_user');

        $user = $this->Auth_model->getAuth($idUser);

        $bulan = $this->post('bulan');
        $tahun = $this->post('tahun');

        $absenTotal = $this->Gaji_model->getCatatan($idUser, $bulan, $tahun);
        $absenMasuk = $this->Gaji_model->getDataAbsenMasuk($idUser, $bulan, $tahun);
        $absenTelat = $this->Gaji_model->getDataAbsenTelat($idUser, $bulan, $tahun);
        $absenMangkir = $this->Gaji_model->getDataAbsenMangkir($idUser, $bulan, $tahun);
        $gajiKaryawan = 5000000;

        if ($absenTelat >= 3 and $absenMangkir >= 3) {
            $gajiKaryawan = $gajiKaryawan - (7 / 100 * 5000000);
        } else {
            if ($absenTelat >= 3) {
                $gajiKaryawan = $gajiKaryawan - (2 / 100 * 5000000);
            } else {
                $gajiKaryawan = $gajiKaryawan;
            }

            if ($absenMangkir >= 3) {
                $gajiKaryawan = $gajiKaryawan - (5 / 100 * 5000000);
            } else {
                $gajiKaryawan = $gajiKaryawan;
            }
        }

        $data = [
            'id_user' => $idUser,
            'username' => $user->username,
            'absen' => $absenTotal,
            'masuk' => $absenMasuk,
            'telat' => $absenTelat,
            'mangkir' => $absenMangkir,
            'bulan' => $bulan,
            'tahun' => $tahun,
            'gaji_total' => $gajiKaryawan
        ];

        if ($this->Gaji_model->cekData($data['id_user'], $data['bulan'], $data['tahun']) == 1) {
            $this->response([
                'status' => false,
                'message' => 'pre-existing data'
            ], 404);
        }

        if ($this->Gaji_model->inserGajiEmploye($data)) {
            $this->response([
                'status' => true,
                'data' => $data,
                'message' => 'Employee salary data added successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Employee salary data failed to be added'
            ], 404);
        }
    }

    function index_delete()
    {
        $id = $this->delete('id_gaji');

        if ($this->Gaji_model->cekIdGaji($id) == 0) {
            $this->response([
                'status' => false,
                'message' => 'attendance id not found'
            ], 404);
        }

        if ($this->Gaji_model->deleteGaji($id) == true) {
            $this->response([
                'status' => true,
                'data' => $id,
                'message' => 'salary deleted successfully'
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'salary failed to delete'
            ], 404);
        }
    }
}
