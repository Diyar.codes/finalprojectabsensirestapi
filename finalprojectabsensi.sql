-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Agu 2020 pada 05.20
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalprojectabsensi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_absensi`
--

CREATE TABLE `tbl_absensi` (
  `id_absensi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `username` text NOT NULL,
  `tgl` int(11) NOT NULL,
  `bulan` text NOT NULL,
  `tahun` int(4) NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_absensi`
--

INSERT INTO `tbl_absensi` (`id_absensi`, `id_user`, `username`, `tgl`, `bulan`, `tahun`, `ket`) VALUES
(1, 4, 'sigit', 1, 'agustus', 2020, 'mangkir'),
(3, 5, 'fandi', 1, 'agustus', 2020, 'masuk'),
(4, 5, 'fandi', 2, 'agustus', 2020, 'masuk'),
(18, 4, 'sigit', 2, 'agustus', 2020, 'masuk'),
(19, 4, 'sigit', 3, 'agustus', 2020, 'masuk'),
(20, 4, 'sigit', 4, 'agustus', 2020, 'masuk'),
(21, 4, 'sigit', 5, 'agustus', 2020, 'masuk'),
(22, 4, 'sigit', 8, 'agustus', 2020, 'masuk'),
(23, 4, 'sigit', 9, 'agustus', 2020, 'masuk'),
(24, 4, 'sigit', 10, 'agustus', 2020, 'telat'),
(25, 4, 'sigit', 11, 'agustus', 2020, 'masuk'),
(26, 4, 'sigit', 12, 'agustus', 2020, 'masuk'),
(27, 4, 'sigit', 15, 'agustus', 2020, 'masuk'),
(28, 4, 'sigit', 16, 'agustus', 2020, 'masuk'),
(29, 4, 'sigit', 17, 'agustus', 2020, 'masuk'),
(30, 4, 'sigit', 18, 'agustus', 2020, 'masuk'),
(31, 4, 'sigit', 19, 'agustus', 2020, 'masuk'),
(32, 4, 'sigit', 22, 'agustus', 2020, 'masuk'),
(33, 4, 'sigit', 23, 'agustus', 2020, 'masuk'),
(34, 4, 'sigit', 24, 'agustus', 2020, 'masuk'),
(35, 4, 'sigit', 25, 'agustus', 2020, 'mangkir'),
(36, 4, 'sigit', 26, 'agustus', 2020, 'masuk'),
(37, 4, 'sigit', 29, 'agustus', 2020, 'masuk'),
(38, 4, 'sigit', 30, 'agustus', 2020, 'masuk'),
(39, 4, 'sigit', 31, 'agustus', 2020, 'masuk'),
(40, 4, 'sigit', 1, 'september', 2020, 'masuk'),
(41, 4, 'sigit', 2, 'september', 2020, 'mangkir'),
(42, 5, 'fandi', 3, 'agustus', 2020, 'masuk'),
(43, 5, 'fandi', 4, 'agustus', 2020, 'masuk'),
(44, 5, 'fandi', 5, 'agustus', 2020, 'masuk'),
(45, 5, 'fandi', 8, 'agustus', 2020, 'masuk'),
(46, 5, 'fandi', 9, 'agustus', 2020, 'masuk'),
(47, 5, 'fandi', 10, 'agustus', 2020, 'masuk'),
(48, 5, 'fandi', 11, 'agustus', 2020, 'masuk'),
(50, 5, 'fandi', 12, 'agustus', 2020, 'masuk'),
(53, 4, 'sigit', 5, 'september', 2020, 'mangkir'),
(55, 4, 'sigit', 6, 'september', 2020, 'mangkir'),
(56, 4, 'sigit', 7, 'september', 2020, 'mangkir'),
(57, 4, 'sigit', 8, 'september', 2020, 'masuk'),
(58, 4, 'sigit', 9, 'september', 2020, 'masuk'),
(59, 4, 'sigit', 20, 'september', 2020, 'masuk'),
(60, 4, 'sigit', 21, 'september', 2020, 'masuk'),
(61, 4, 'sigit', 22, 'september', 2020, 'masuk'),
(62, 4, 'sigit', 23, 'september', 2020, 'telat'),
(63, 4, 'sigit', 24, 'september', 2020, 'telat'),
(64, 4, 'sigit', 27, 'september', 2020, 'telat'),
(65, 4, 'sigit', 28, 'september', 2020, 'masuk'),
(66, 4, 'sigit', 29, 'september', 2020, 'masuk'),
(67, 4, 'sigit', 30, 'september', 2020, 'mangkir'),
(68, 5, 'fandi', 1, 'september', 2020, 'masuk'),
(69, 5, 'fandi', 3, 'september', 2020, 'masuk'),
(70, 5, 'fandi', 4, 'september', 2020, 'mangkir'),
(71, 5, 'fandi', 5, 'september', 2020, 'mangkir'),
(72, 5, 'fandi', 6, 'september', 2020, 'telat'),
(73, 5, 'fandi', 7, 'september', 2020, 'mangkir'),
(74, 5, 'fandi', 9, 'september', 2020, 'mangkir'),
(75, 5, 'fandi', 10, 'september', 2020, 'masuk'),
(76, 5, 'fandi', 11, 'september', 2020, 'masuk'),
(77, 5, 'fandi', 12, 'september', 2020, 'masuk'),
(78, 5, 'fandi', 13, 'september', 2020, 'telat'),
(79, 5, 'fandi', 15, 'september', 2020, 'masuk'),
(80, 5, 'fandi', 16, 'september', 2020, 'masuk'),
(81, 5, 'fandi', 17, 'september', 2020, 'masuk'),
(82, 5, 'fandi', 26, 'september', 2020, 'masuk'),
(83, 5, 'fandi', 27, 'september', 2020, 'masuk'),
(84, 5, 'fandi', 28, 'september', 2020, 'masuk'),
(85, 5, 'fandi', 29, 'september', 2020, 'masuk'),
(87, 5, 'fandi', 31, 'september', 2020, 'mangkir'),
(88, 55, 'cus', 1, 'agustus', 2020, 'masuk'),
(89, 55, 'cus', 2, 'agustus', 2020, 'masuk'),
(90, 55, 'cus', 3, 'agustus', 2020, 'masuk'),
(91, 55, 'cus', 4, 'agustus', 2020, 'masuk'),
(92, 55, 'cus', 5, 'agustus', 2020, 'masuk'),
(93, 55, 'cus', 8, 'agustus', 2020, 'masuk'),
(94, 55, 'cus', 9, 'agustus', 2020, 'masuk'),
(95, 55, 'cus', 10, 'agustus', 2020, 'mangkir'),
(96, 55, 'cus', 11, 'agustus', 2020, 'mangkir'),
(97, 55, 'cus', 12, 'agustus', 2020, 'mangkir'),
(98, 55, 'cus', 15, 'agustus', 2020, 'masuk'),
(101, 55, 'cus', 1, 'september', 2020, 'masuk'),
(102, 55, 'cus', 2, 'september', 2020, 'masuk'),
(103, 55, 'cus', 3, 'september', 2020, 'masuk'),
(104, 55, 'cus', 4, 'september', 2020, 'mangkir'),
(105, 55, 'cus', 6, 'september', 2020, 'mangkir'),
(106, 55, 'cus', 26, 'september', 2020, 'masuk'),
(107, 55, 'cus', 27, 'september', 2020, 'mangkir'),
(108, 55, 'cus', 28, 'september', 2020, 'masuk'),
(109, 55, 'cus', 29, 'september', 2020, 'masuk'),
(111, 55, 'cus', 30, 'september', 2020, 'masuk'),
(112, 4, 'sigit', 1, 'oktober', 2020, 'masuk'),
(113, 4, 'sigit', 2, 'oktober', 2020, 'masuk'),
(115, 4, 'sigit', 3, 'oktober', 2020, 'masuk'),
(116, 4, 'sigit', 4, 'oktober', 2020, 'masuk'),
(117, 4, 'sigit', 5, 'oktober', 2020, 'masuk'),
(118, 4, 'sigit', 8, 'oktober', 2020, 'masuk'),
(119, 4, 'sigit', 9, 'oktober', 2020, 'masuk'),
(120, 4, 'sigit', 10, 'oktober', 2020, 'masuk'),
(121, 4, 'sigit', 11, 'oktober', 2020, 'masuk'),
(122, 4, 'sigit', 12, 'oktober', 2020, 'masuk'),
(123, 4, 'sigit', 15, 'oktober', 2020, 'masuk'),
(124, 4, 'sigit', 16, 'oktober', 2020, 'masuk'),
(125, 4, 'sigit', 17, 'oktober', 2020, 'masuk'),
(126, 4, 'sigit', 18, 'oktober', 2020, 'masuk'),
(127, 4, 'sigit', 19, 'oktober', 2020, 'masuk'),
(128, 4, 'sigit', 22, 'oktober', 2020, 'masuk'),
(129, 4, 'sigit', 23, 'oktober', 2020, 'masuk'),
(130, 4, 'sigit', 24, 'oktober', 2020, 'masuk'),
(131, 4, 'sigit', 25, 'oktober', 2020, 'masuk'),
(133, 4, 'sigit', 26, 'oktober', 2020, 'masuk'),
(134, 4, 'sigit', 29, 'oktober', 2020, 'masuk'),
(135, 4, 'sigit', 30, 'oktober', 2020, 'masuk'),
(137, 4, 'sigit', 31, 'oktober', 2020, 'telat'),
(145, 4, 'sigit', 1, 'november', 2020, 'masuk'),
(146, 4, 'sigit', 2, 'november', 2020, 'masuk'),
(148, 4, 'sigit', 3, 'november', 2020, 'masuk'),
(149, 4, 'sigit', 4, 'november', 2020, 'masuk'),
(150, 4, 'sigit', 5, 'november', 2020, 'masuk'),
(151, 4, 'sigit', 8, 'november', 2020, 'telat'),
(152, 4, 'sigit', 9, 'november', 2020, 'telat'),
(153, 4, 'sigit', 10, 'november', 2020, 'telat'),
(154, 4, 'sigit', 11, 'november', 2020, 'telat'),
(155, 4, 'sigit', 12, 'november', 2020, 'telat'),
(156, 4, 'sigit', 15, 'november', 2020, 'mangkir'),
(157, 4, 'sigit', 16, 'november', 2020, 'mangkir'),
(159, 4, 'sigit', 17, 'november', 2020, 'mangkir'),
(160, 4, 'sigit', 18, 'november', 2020, 'mangkir'),
(161, 4, 'sigit', 19, 'november', 2020, 'mangkir'),
(162, 4, 'sigit', 22, 'november', 2020, 'masuk'),
(163, 4, 'sigit', 23, 'november', 2020, 'masuk'),
(164, 4, 'sigit', 24, 'november', 2020, 'masuk'),
(165, 4, 'sigit', 25, 'november', 2020, 'masuk'),
(166, 4, 'sigit', 26, 'november', 2020, 'masuk'),
(167, 4, 'sigit', 29, 'november', 2020, 'masuk'),
(168, 4, 'sigit', 30, 'november', 2020, 'masuk'),
(169, 4, 'sigit', 31, 'november', 2020, 'masuk'),
(170, 5, 'fandi', 1, 'desember', 2020, 'masuk'),
(178, 55, 'cus', 1, 'oktober', 2020, 'masuk'),
(179, 55, 'cus', 2, 'oktober', 2020, 'masuk'),
(180, 55, 'cus', 3, 'oktober', 2020, 'masuk'),
(181, 55, 'cus', 4, 'oktober', 2020, 'masuk'),
(182, 55, 'cus', 5, 'oktober', 2020, 'masuk'),
(183, 55, 'cus', 8, 'oktober', 2020, 'telat'),
(184, 55, 'cus', 9, 'oktober', 2020, 'telat'),
(185, 55, 'cus', 10, 'oktober', 2020, 'masuk'),
(186, 55, 'cus', 11, 'oktober', 2020, 'telat'),
(187, 55, 'cus', 12, 'oktober', 2020, 'telat'),
(188, 55, 'cus', 15, 'oktober', 2020, 'mangkir'),
(189, 55, 'cus', 16, 'oktober', 2020, 'mangkir'),
(190, 55, 'cus', 17, 'oktober', 2020, 'mangkir'),
(191, 55, 'cus', 18, 'oktober', 2020, 'mangkir'),
(192, 55, 'cus', 19, 'oktober', 2020, 'mangkir'),
(193, 55, 'cus', 22, 'oktober', 2020, 'masuk'),
(194, 55, 'cus', 23, 'oktober', 2020, 'masuk'),
(195, 55, 'cus', 24, 'oktober', 2020, 'masuk'),
(196, 55, 'cus', 25, 'oktober', 2020, 'masuk'),
(197, 55, 'cus', 26, 'oktober', 2020, 'masuk'),
(198, 55, 'cus', 29, 'oktober', 2020, 'masuk'),
(199, 55, 'cus', 30, 'oktober', 2020, 'masuk'),
(200, 55, 'cus', 31, 'oktober', 2020, 'masuk'),
(201, 55, 'cus', 1, 'november', 2020, 'masuk'),
(202, 55, 'cus', 2, 'november', 2020, 'masuk'),
(203, 55, 'cus', 3, 'november', 2020, 'telat'),
(204, 55, 'cus', 4, 'november', 2020, 'telat'),
(205, 55, 'cus', 5, 'november', 2020, 'telat'),
(206, 55, 'cus', 8, 'november', 2020, 'masuk'),
(207, 55, 'cus', 9, 'november', 2020, 'masuk'),
(208, 55, 'cus', 10, 'november', 2020, 'masuk'),
(209, 55, 'cus', 11, 'november', 2020, 'mangkir'),
(210, 55, 'cus', 12, 'november', 2020, 'mangkir'),
(211, 55, 'cus', 15, 'november', 2020, 'masuk'),
(212, 55, 'cus', 16, 'november', 2020, 'masuk'),
(213, 55, 'cus', 17, 'november', 2020, 'telat'),
(214, 55, 'cus', 18, 'november', 2020, 'masuk'),
(215, 55, 'cus', 19, 'november', 2020, 'masuk'),
(216, 55, 'cus', 22, 'november', 2020, 'masuk'),
(217, 55, 'cus', 23, 'november', 2020, 'masuk'),
(218, 55, 'cus', 24, 'november', 2020, 'masuk'),
(219, 55, 'cus', 25, 'november', 2020, 'masuk'),
(220, 55, 'cus', 26, 'november', 2020, 'telat'),
(221, 55, 'cus', 29, 'november', 2020, 'masuk'),
(222, 55, 'cus', 30, 'november', 2020, 'masuk'),
(223, 55, 'cus', 31, 'november', 2020, 'masuk'),
(224, 55, 'cus', 1, 'desember', 2020, 'masuk'),
(225, 55, 'cus', 2, 'desember', 2020, 'masuk'),
(226, 55, 'cus', 3, 'desember', 2020, 'masuk'),
(227, 55, 'cus', 4, 'desember', 2020, 'masuk'),
(228, 55, 'cus', 5, 'desember', 2020, 'masuk'),
(229, 55, 'cus', 8, 'desember', 2020, 'masuk'),
(230, 55, 'cus', 9, 'desember', 2020, 'telat'),
(231, 55, 'cus', 10, 'desember', 2020, 'mangkir'),
(232, 55, 'cus', 11, 'desember', 2020, 'telat'),
(233, 55, 'cus', 12, 'desember', 2020, 'mangkir'),
(234, 55, 'cus', 15, 'desember', 2020, 'masuk'),
(235, 55, 'cus', 16, 'desember', 2020, 'masuk'),
(236, 55, 'cus', 17, 'desember', 2020, 'masuk'),
(237, 55, 'cus', 18, 'desember', 2020, 'masuk'),
(238, 55, 'cus', 19, 'desember', 2020, 'masuk'),
(239, 55, 'cus', 22, 'desember', 2020, 'masuk'),
(240, 55, 'cus', 23, 'desember', 2020, 'masuk'),
(241, 55, 'cus', 24, 'desember', 2020, 'masuk'),
(242, 55, 'cus', 25, 'desember', 2020, 'masuk'),
(243, 55, 'cus', 26, 'desember', 2020, 'masuk'),
(244, 55, 'cus', 28, 'desember', 2020, 'masuk'),
(245, 55, 'cus', 29, 'desember', 2020, 'masuk'),
(246, 55, 'cus', 30, 'desember', 2020, 'masuk'),
(247, 55, 'cus', 31, 'desember', 2020, 'masuk'),
(249, 4, 'sigit', 1, 'desember', 2020, 'masuk'),
(250, 4, 'sigit', 2, 'desember', 2020, 'masuk'),
(251, 4, 'sigit', 3, 'desember', 2020, 'masuk'),
(252, 4, 'sigit', 4, 'desember', 2020, 'masuk'),
(253, 4, 'sigit', 5, 'desember', 2020, 'telat'),
(254, 4, 'sigit', 8, 'desember', 2020, 'masuk'),
(255, 4, 'sigit', 9, 'desember', 2020, 'masuk'),
(256, 4, 'sigit', 10, 'desember', 2020, 'masuk'),
(257, 4, 'sigit', 11, 'desember', 2020, 'masuk'),
(258, 4, 'sigit', 12, 'desember', 2020, 'masuk'),
(259, 4, 'sigit', 15, 'desember', 2020, 'masuk'),
(260, 4, 'sigit', 16, 'desember', 2020, 'masuk'),
(261, 4, 'sigit', 17, 'desember', 2020, 'masuk'),
(262, 4, 'sigit', 18, 'desember', 2020, 'masuk'),
(263, 4, 'sigit', 19, 'desember', 2020, 'masuk'),
(264, 4, 'sigit', 22, 'desember', 2020, 'masuk'),
(265, 4, 'sigit', 23, 'desember', 2020, 'masuk'),
(266, 4, 'sigit', 24, 'desember', 2020, 'masuk'),
(267, 4, 'sigit', 25, 'desember', 2020, 'masuk'),
(268, 4, 'sigit', 26, 'desember', 2020, 'masuk'),
(269, 4, 'sigit', 29, 'desember', 2020, 'masuk'),
(270, 4, 'sigit', 30, 'desember', 2020, 'masuk'),
(271, 4, 'sigit', 31, 'desember', 2020, 'masuk');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_complain`
--

CREATE TABLE `tbl_complain` (
  `id_complain` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `username` text NOT NULL,
  `pesan` text NOT NULL,
  `id_gaji` int(11) NOT NULL,
  `bulan` text NOT NULL,
  `tahun` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_complain`
--

INSERT INTO `tbl_complain` (`id_complain`, `id_user`, `username`, `pesan`, `id_gaji`, `bulan`, `tahun`) VALUES
(20, 5, 'fandi', 'sebenarnya saya hanya mangkir 2 kali pada bulan ini', 14, 'agustus', 2020),
(22, 5, 'fandi', 'oke pak ', 19, 'september', 2020),
(81, 4, 'sigit', 'oke sip pak makasih banyak', 13, 'agustus', 2020),
(82, 4, 'sigit', 'sepertinya saya ijin 3 kali, jadi saya cuma mangkir 2 kali pak, coba cek ulang surat kesehatan saya', 18, 'september', 2020),
(84, 4, 'sigit', 'oke', 35, 'november', 2020),
(85, 4, 'sigit', 'cascas', 13, 'agustus', 2020),
(89, 55, 'cus', 'saya hanya mangkir 1 kali coba lah ', 42, 'agustus', 2020),
(90, 55, 'cus', 'saya hanya mangkir 2 kali, dan bulan kemarin belum diperbarui\n', 43, 'september', 2020),
(92, 55, 'cus', 'saya ijin 2 kali', 45, 'november', 2020);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_gaji`
--

CREATE TABLE `tbl_gaji` (
  `id_gaji` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `username` text NOT NULL,
  `absen` int(11) NOT NULL,
  `masuk` int(11) NOT NULL,
  `telat` int(11) NOT NULL,
  `mangkir` int(11) NOT NULL,
  `bulan` text NOT NULL,
  `tahun` int(4) NOT NULL,
  `gaji_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_gaji`
--

INSERT INTO `tbl_gaji` (`id_gaji`, `id_user`, `username`, `absen`, `masuk`, `telat`, `mangkir`, `bulan`, `tahun`, `gaji_total`) VALUES
(13, 4, 'sigit', 23, 20, 1, 2, 'agustus', 2020, 5000000),
(18, 4, 'sigit', 16, 8, 3, 5, 'september', 2020, 4650000),
(29, 4, 'sigit', 23, 22, 1, 0, 'oktober', 2020, 5000000),
(35, 4, 'sigit', 23, 13, 5, 5, 'november', 2020, 4650000),
(42, 55, 'cus', 11, 8, 0, 3, 'agustus', 2020, 4750000),
(43, 55, 'cus', 10, 7, 0, 3, 'september', 2020, 4750000),
(44, 55, 'cus', 23, 14, 4, 5, 'oktober', 2020, 4650000),
(58, 61, 'ahmadi', 6, 3, 2, 1, 'agustus', 2020, 5000000),
(59, 55, 'cus', 24, 20, 2, 2, 'desember', 2020, 5000000),
(60, 4, 'sigit', 23, 22, 1, 0, 'desember', 2020, 5000000),
(67, 5, 'fandi', 10, 10, 0, 0, 'agustus', 2020, 5000000),
(68, 5, 'fandi', 19, 12, 2, 5, 'september', 2020, 4750000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id_user` int(11) NOT NULL,
  `username` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `level` enum('finance','karyawan') NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`id_user`, `username`, `email`, `password`, `level`, `image`) VALUES
(4, 'sigit', 'Sigit@gmail.com', '$2y$10$MC1p1/t3JhadrS83ZAYL3.Ih94qyda0wxeYR.Rb/1WfV3CNXI5cx2', 'karyawan', 'download.jpg'),
(5, 'fandi', 'fandi@gmail.com', '$2y$10$ViaACJiOqEA.MjuLSEWPK.W/.ek10ZICoPVYpiSVLpkdVU7kIg89S', 'karyawan', 'default.png'),
(55, 'cus', 'cus@gmail.com', '$2y$10$ogyvgm29gZyCD6MwnOKaJOLa58Uy9.E/936QHeNSbmp3Alm.Im8jG', 'karyawan', '416x416.jpg'),
(57, 'diyar.codesss', 'diyar@gmail.com', '$2y$10$7B7Vx1cWwSdVp1cJyQIYKuqQbqnEjyT90Vd9er6IFfaz2JDxoviTS', 'finance', '416x4161.jpg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_absensi`
--
ALTER TABLE `tbl_absensi`
  ADD PRIMARY KEY (`id_absensi`);

--
-- Indeks untuk tabel `tbl_complain`
--
ALTER TABLE `tbl_complain`
  ADD PRIMARY KEY (`id_complain`);

--
-- Indeks untuk tabel `tbl_gaji`
--
ALTER TABLE `tbl_gaji`
  ADD PRIMARY KEY (`id_gaji`);

--
-- Indeks untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_absensi`
--
ALTER TABLE `tbl_absensi`
  MODIFY `id_absensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=275;

--
-- AUTO_INCREMENT untuk tabel `tbl_complain`
--
ALTER TABLE `tbl_complain`
  MODIFY `id_complain` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT untuk tabel `tbl_gaji`
--
ALTER TABLE `tbl_gaji`
  MODIFY `id_gaji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT untuk tabel `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
